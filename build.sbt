import AssemblyKeys._

name := "hwo2014bot"

scalaVersion := "2.10.4"

scalacOptions ++= Seq(
  "-feature",
  "-unchecked",
  "-deprecation",
  "-Yno-adapted-args",
  "-language:implicitConversions",
  "-language:higherKinds"
)

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.0.9",
  "org.scalatest" %% "scalatest" % "2.0" % "test"
)

resolvers += "sonatype-public" at "https://oss.sonatype.org/content/groups/public"

assemblySettings

jarName in assembly := "hwo2014bot.jar"

mainClass in assembly := Some("hwo2014.Monopost")

test in assembly := {}

cancelable := true
