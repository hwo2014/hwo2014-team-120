package hwo2014

import org.slf4j.LoggerFactory
import ServerMsg._

class Driver {
  val logger = LoggerFactory.getLogger("Driver")

  var trackLanes = Vector.empty[Lane]
  var trackPieces = Vector.empty[Piece]
  var myCar: CarId = _
  var turning = false

  def react(message: ServerMsg): BotMsg = {
    message match {
      case m: JoinRace       => joinRace(m)
      case m: Join           => join(m)
      case m: YourCar        => yourCar(m)
      case m: GameInit       => gameInit(m)
      case m: GameStart      => gameStart(m)
      case m: CarPositions   => carPositions(m)
      case m: GameEnd        => gameEnd(m)
      case m: TournamentEnd  => tournamentEnd(m)
      case m: Crash          => crash(m)
      case m: Spawn          => spawn(m)
      case m: LapFinished    => lapFinished(m)
      case m: Dnf            => dnf(m)
      case m: Finish         => finish(m)
      case m: TurboAvailable => turboAvailable(m)
    }
  }

  private def joinRace(message: JoinRace): BotMsg = {
    BotMsg.Ping(None)
  }

  private def join(message: Join): BotMsg = {
    BotMsg.Ping(None)
  }

  private def yourCar(message: YourCar): BotMsg = {
    myCar = CarId(message.name, message.color)
    BotMsg.Ping(None)
  }

  private def gameInit(message: GameInit): BotMsg = {
    trackPieces = message.track.pieces.toVector
    trackLanes = message.track.lanes.toVector
    BotMsg.Ping(None)
  }

  private def gameStart(message: GameStart): BotMsg = {
    BotMsg.Throttle(0.1, message.gameTick)
  }

  private def carPositions(message: CarPositions): BotMsg = {
    val myPosition = message.positions.find(_.id == myCar).getOrElse {
      sys.error("My car isn't listed in positions?")
    }

    if (turnFollows(myPosition)) {
      if (onInteriorTurn(myPosition) && canTurn(myPosition) && !turning) {
        exteriorDirection(myPosition) match {
          case Some(direction) =>
            logger.debug(s"Car on interior before turn piece. Switching lanes to $direction")
            turning = true
            BotMsg.SwitchLane(direction, message.gameTick)
          case None =>
            logger.warn(s"Couldn't determine exterior direction for position: $myPosition and lanes $trackLanes")
            BotMsg.Ping(message.gameTick)
        }
      } else {
        val throttle = 0.4
        logger.debug(s"Turn piece follows. Decelerating to $throttle throttle.")
        BotMsg.Throttle(throttle, message.gameTick) // decelerate
      }
    } else {
      turning = false
      BotMsg.Throttle(1.0, message.gameTick) // full speed
    }
  }

  private def gameEnd(message: GameEnd): BotMsg = {
    BotMsg.Ping(None)
  }

  private def tournamentEnd(message: TournamentEnd): BotMsg = {
    BotMsg.Ping(None)
  }

  private def crash(message: Crash): BotMsg = {
    logger.error(s"crash: $message")
    BotMsg.Ping(message.gameTick)
  }

  private def spawn(message: Spawn): BotMsg = {
    BotMsg.Ping(message.gameTick)
  }

  private def lapFinished(message: LapFinished): BotMsg = {
    BotMsg.Ping(message.gameTick)
  }

  private def dnf(message: Dnf): BotMsg = {
    BotMsg.Ping(message.gameTick)
  }

  private def finish(message: Finish): BotMsg = {
    BotMsg.Ping(message.gameTick)
  }

  private def turboAvailable(message: TurboAvailable): BotMsg = {
    BotMsg.Ping(message.gameTick)
  }

  def turnFollows(position: CarPosition): Boolean = {
    trackPieces.lift(position.piecePosition.pieceIndex + 1).map {
      case _: StraightPiece => false
      case _: BentPiece     => true
    } getOrElse false
  }

  def exteriorDirection(position: CarPosition): Option[Direction] = {
    trackLanes.find(_.index == position.piecePosition.lane.startLaneIndex) .map { lane =>
      if (lane.distanceFromCenter < 0) Direction.Right else Direction.Left
    }
  }

  def onInteriorTurn(position: CarPosition): Boolean = {
    trackPieces.lift(position.piecePosition.pieceIndex + 1).map {
      case s: StraightPiece => false
      case b: BentPiece =>
        if (b.angle < 0) { // left
          trackLanes.find(_.index == position.piecePosition.lane.startLaneIndex)
            .map { lane =>
              lane.distanceFromCenter < 0
            }.getOrElse(false)
        } else { // right
          trackLanes.find(_.index == position.piecePosition.lane.startLaneIndex)
            .map(_.distanceFromCenter > 0)
            .getOrElse(false)
        }
    } getOrElse false
  }

  def canTurn(position: CarPosition) = {
    trackPieces.lift(position.piecePosition.pieceIndex)
      .map(_.switch.getOrElse(false))
      .getOrElse(false)
  }
}
