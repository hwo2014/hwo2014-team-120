package hwo2014

import java.net.Socket
import java.io.{ BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter }
import scala.annotation.tailrec
import argonaut._, Argonaut._
import scalaz.{ -\/, \/- }
import org.slf4j.LoggerFactory

object Monopost {
  private val logger   = LoggerFactory.getLogger("Monopost")
  private val botName  = "monopost"
  private val botKey   = "rtN2hlnLPIATXw"
  private val testHost = "testserver.helloworldopen.com"
  private val testPort = 8091

  def main(args: Array[String]): Unit = {
    args.toList match {
      case host :: port :: _ => start(host, port.toInt, botName, botKey, new Driver)
      case _                 => start(testHost, testPort, botName, botKey, new Driver)
    }
  }

  def start(host: String, port: Int, botName: String, botKey: String, driver: Driver): Unit = {
    val socket = new Socket(host, port)
    val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF-8"))
    val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF-8"))

    def send[A: EncodeJson](a: A): Unit = {
      val line = a.asJson.nospaces
      writer.println(line)
      writer.flush
      logger.debug(s"sent: $line")
    }

    def line(): String = {
      val line = reader.readLine()
      logger.debug(s"read: $line")
      line
    }

    @tailrec
    def loop(message: BotMsg): Unit = {
      send(message)

      line() match {
        case null => logger.debug("eof received.")
        case line =>
          line.decode[ServerMsg] match {
            case \/-(message) =>
              loop(driver.react(message))
            case -\/(errors)  =>
              logger.error(s"error decoding incoming line: $line $errors")
              loop(BotMsg.Ping(None))
          }
      }
    }

    loop(BotMsg.Join(botName, botKey))
  }
}
