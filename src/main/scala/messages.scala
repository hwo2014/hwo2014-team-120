package hwo2014

import argonaut._, Argonaut._

sealed trait Direction
object Direction {
  case object Left extends Direction
  case object Right extends Direction

  implicit val ArgonautEncoder: EncodeJson[Direction] =
    EncodeJson {
      case Left  => jString("Left")
      case Right => jString("Right")
    }
}

case class BotId(name: String, key: String)
object BotId {
  implicit val ArgonautEncoder = casecodec2(BotId.apply, BotId.unapply)("name", "key")
}

sealed trait BotMsg
object BotMsg {
  case class JoinRace(botId: BotId, trackName: Option[String] = None, password: Option[String] = None, carCount: Int = 1) extends BotMsg
  case class Join(name: String, key: String) extends BotMsg
  case class Throttle(percent: Double, gameTick: Option[Int]) extends BotMsg
  case class SwitchLane(direction: Direction, gameTick: Option[Int]) extends BotMsg
  case class Turbo(message: String = "Turbo ON!") extends BotMsg
  case class Ping(gameTick: Option[Int]) extends BotMsg

  implicit val ArgonautEncoder: EncodeJson[BotMsg] =
    EncodeJson {
      case JoinRace(botId, trackName, password, carCount) =>
        Json.obj(
          "msgType" := "joinRace",
          "data"    := Json.obj(
            "botId"     := botId,
            "trackName" := trackName,
            "password"  := password,
            "carCount"  := carCount
        ))
      case Join(name, key)                 => Json.obj("msgType" := "join", "data" := Json.obj("name" := name, "key" := key))
      case Throttle(percent, gameTick)     => Json.obj("msgType" := "throttle", "data" := percent, "gameTick" := gameTick)
      case SwitchLane(direction, gameTick) => Json.obj("msgType" := "switchLane", "data" := direction, "gameTick" := gameTick)
      case Turbo(message)                  => Json.obj("msgType" := "turbo", "data" := message)
      case Ping(gameTick)                  => Json.obj("msgType" := "ping", "gameTick" := gameTick)
    }
}

sealed trait Piece {
  val switch: Option[Boolean]
}
case class StraightPiece(length: Double, switch: Option[Boolean]) extends Piece
case class BentPiece(radius: Double, angle: Double, switch: Option[Boolean]) extends Piece

object Piece {
  implicit val ArgonautDecoder: DecodeJson[Piece] =
    jdecode2L(StraightPiece.apply)("length", "switch") ||| jdecode3L(BentPiece.apply)("radius", "angle", "switch")
}

case class Lane(distanceFromCenter: Double, index: Int)
object Lane {
  implicit val ArgonautDecoder = jdecode2L(Lane.apply)("distanceFromCenter", "index")
}

case class Position(x: Double, y: Double)
object Position {
  implicit val ArgonautDecoder = jdecode2L(Position.apply)("x", "y")
}

case class Track(id: String, name: String, pieces: List[Piece], lanes: List[Lane])
object Track {
  implicit val ArgonautDecoder = jdecode4L(Track.apply)("id", "name", "pieces", "lanes")
}

case class CarId(name: String, color: String)
object CarId {
  implicit val ArgonautDecoder = jdecode2L(CarId.apply)("name", "color")
}

case class Dimensions(length: Double, width: Double, guideFlagPosition: Double)
object Dimensions {
  implicit val ArgonautDecoder = jdecode3L(Dimensions.apply)("length", "width", "guideFlagPosition")
}

case class Car(id: CarId, dimensions: Dimensions)
object Car {
  implicit val ArgonautDecoder = jdecode2L(Car.apply)("id", "dimensions")
}

case class RaceSession(laps: Int, maxLapTimeMs: Long, quickRace: Boolean)
object RaceSession {
  implicit val ArgonautDecoder = jdecode3L(RaceSession.apply)("laps", "maxLapTimeMs", "quickRace")
}

case class LanePosition(startLaneIndex: Int, endLaneIndex: Int)
object LanePosition {
  implicit val ArgonautDecoder = jdecode2L(LanePosition.apply)("startLaneIndex", "endLaneIndex")
}

case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: LanePosition, lap: Int)
object PiecePosition {
  implicit val ArgonautDecoder = jdecode4L(PiecePosition.apply)("pieceIndex", "inPieceDistance", "lane", "lap")
}

case class CarPosition(id: CarId, angle: Double, piecePosition: PiecePosition)
object CarPosition {
  implicit val ArgonautDecoder = jdecode3L(CarPosition.apply)("id", "angle", "piecePosition")
}

case class LapStats(laps: Int, ticks: Long, millis: Long)
object LapStats {
  implicit val ArgonautDecoder = jdecode3L(LapStats.apply)("lap", "ticks", "millis")
}

case class RaceStats(laps: Int, ticks: Long, millis: Long)
object RaceStats {
  implicit val ArgonautDecoder = jdecode3L(RaceStats.apply)("laps", "ticks", "millis")
}

case class Rank(overall: Int, fastestLap: Int)
object Rank {
  implicit val ArgonautDecoder = jdecode2L(Rank.apply)("overall", "fastestLap")
}

case class ResultData(lap: Option[Int], ticks: Option[Long], millis: Option[Long])
object ResultData {
  implicit val ArgonautDecoder = jdecode3L(ResultData.apply)("lap", "ticks", "millis")
}

case class Result(carId: CarId, result: ResultData)
object Result {
  implicit val ArgonautDecoder = jdecode2L(Result.apply)("car", "result")
}

sealed trait ServerMsg
object ServerMsg {
  case class JoinRace(botId: BotId, trackName: String, carCount: Int) extends ServerMsg
  case class Join(name: String, key: String) extends ServerMsg
  case class YourCar(name: String, color: String) extends ServerMsg
  case class GameInit(track: Track, cars: List[Car], raceSession: RaceSession) extends ServerMsg
  case class GameStart(gameId: String, gameTick: Option[Int]) extends ServerMsg
  case class CarPositions(positions: List[CarPosition], gameId: String, gameTick: Option[Int]) extends ServerMsg
  case class GameEnd(results: List[Result], bestLaps: List[Result]) extends ServerMsg
  case class TournamentEnd() extends ServerMsg
  case class Crash(carId: CarId, gameId: String, gameTick: Option[Int]) extends ServerMsg
  case class Spawn(carId: CarId, gameId: String, gameTick: Option[Int]) extends ServerMsg
  case class LapFinished(carId: CarId, lapTime: LapStats, raceTime: RaceStats, ranking: Rank, gameId: String, gameTick: Option[Int]) extends ServerMsg
  case class Dnf(carId: CarId, reason: String, gameId: String, gameTick: Option[Int]) extends ServerMsg
  case class Finish(carId: CarId, gameId: String, gameTick: Option[Int]) extends ServerMsg
  case class TurboAvailable(durationMilliseconds: Double, durationTicks: Int, factor: Double, gameId: String, gameTick: Option[Int]) extends ServerMsg

  def decodeJoinRace(cursor: HCursor) = {
    for {
      botId     <- cursor.downField("data").get[BotId]("botId")
      trackName <- cursor.downField("data").get[String]("trackName")
      carCount  <- cursor.downField("data").get[Int]("carCount")
    } yield JoinRace(botId, trackName, carCount)
  }

  def decodeJoin(cursor: HCursor) = {
    for {
      name <- cursor.downField("data").get[String]("name")
      key  <- cursor.downField("data").get[String]("key")
    } yield Join(name, key)
  }

  def decodeYourCar(cursor: HCursor) =
    for {
      name  <- cursor.downField("data").get[String]("name")
      color <- cursor.downField("data").get[String]("color")
    } yield YourCar(name, color)

  def decodeGameInit(cursor: HCursor) =
    for {
      track       <- cursor.downField("data").downField("race").get[Track]("track")
      cars        <- cursor.downField("data").downField("race").get[List[Car]]("cars")
      raceSession <- cursor.downField("data").downField("race").get[RaceSession]("raceSession")
    } yield GameInit(track, cars, raceSession)

  def decodeGameStart(cursor: HCursor) =
    for {
      gameId   <- cursor.get[String]("gameId")
      gameTick <- cursor.get[Option[Int]]("gameTick")
    } yield GameStart(gameId, gameTick)

  def decodeCarPositions(cursor: HCursor) =
    for {
      positions <- cursor.get[List[CarPosition]]("data")
      gameId    <- cursor.get[String]("gameId")
      gameTick  <- cursor.get[Option[Int]]("gameTick")
    } yield CarPositions(positions, gameId, gameTick)

  def decodeGameEnd(cursor: HCursor) =
    for {
      results  <- cursor.downField("data").get[List[Result]]("results")
      bestLaps <- cursor.downField("data").get[List[Result]]("bestLaps")
    } yield GameEnd(results, bestLaps)

  def decodeCrash(cursor: HCursor) =
    for {
      carId     <- cursor.get[CarId]("data")
      gameId    <- cursor.get[String]("gameId")
      gameTick  <- cursor.get[Option[Int]]("gameTick")
    } yield Crash(carId, gameId, gameTick)

  def decodeSpawn(cursor: HCursor) =
    for {
      carId     <- cursor.get[CarId]("data")
      gameId    <- cursor.get[String]("gameId")
      gameTick  <- cursor.get[Option[Int]]("gameTick")
    } yield Spawn(carId, gameId, gameTick)

  def decodeLapFinished(cursor: HCursor) =
    for {
      carId    <- cursor.downField("data").get[CarId]("car")
      lapTime  <- cursor.downField("data").get[LapStats]("lapTime")
      raceTime <- cursor.downField("data").get[RaceStats]("raceTime")
      ranking  <- cursor.downField("data").get[Rank]("ranking")
      gameId   <- cursor.get[String]("gameId")
      gameTick <- cursor.get[Option[Int]]("gameTick")
    } yield LapFinished(carId, lapTime, raceTime, ranking, gameId, gameTick)

  def decodeDnf(cursor: HCursor) =
    for {
      carId    <- cursor.downField("data").get[CarId]("car")
      reason   <- cursor.downField("data").get[String]("reason")
      gameId   <- cursor.get[String]("gameId")
      gameTick <- cursor.get[Option[Int]]("gameTick")
    } yield Dnf(carId, reason, gameId, gameTick)

  def decodeFinish(cursor: HCursor) =
    for {
      carId     <- cursor.get[CarId]("data")
      gameId    <- cursor.get[String]("gameId")
      gameTick  <- cursor.get[Option[Int]]("gameTick")
    } yield Finish(carId, gameId, gameTick)

  def decodeTurboAvailable(cursor: HCursor) =
    for {
      durationMilliseconds <- cursor.downField("data").get[Double]("turboDurationMilliseconds")
      durationTicks        <- cursor.downField("data").get[Int]("turboDurationTicks")
      factor               <- cursor.downField("data").get[Double]("turboFactor")
      gameId               <- cursor.get[String]("gameId")
      gameTick             <- cursor.get[Option[Int]]("gameTick")
    } yield TurboAvailable(durationMilliseconds, durationTicks, factor, gameId, gameTick)

  implicit val ArgonautDecoder: DecodeJson[ServerMsg] =
    DecodeJson { cursor =>
      cursor.get[String]("msgType").flatMap {
        case "joinRace"       => decodeJoinRace(cursor)
        case "join"           => decodeJoin(cursor)
        case "yourCar"        => decodeYourCar(cursor)
        case "gameInit"       => decodeGameInit(cursor)
        case "gameStart"      => decodeGameStart(cursor)
        case "carPositions"   => decodeCarPositions(cursor)
        case "gameEnd"        => decodeGameEnd(cursor)
        case "tournamentEnd"  => DecodeResult.ok(TournamentEnd())
        case "crash"          => decodeCrash(cursor)
        case "spawn"          => decodeSpawn(cursor)
        case "lapFinished"    => decodeLapFinished(cursor)
        case "dnf"            => decodeDnf(cursor)
        case "finish"         => decodeFinish(cursor)
        case "turboAvailable" => decodeTurboAvailable(cursor)
        case msg              => DecodeResult.fail[ServerMsg](s"Invalid message type $msg", cursor.history)
      }
    }
}
